---
layout: post
title: About
skip_related: true
---


I'm a cybersecurity enthusiast deeply passionate about threat intelligence, risk management, and compliance. I thrive on diving into code, analyzing data, and exploring the latest advancements in AI.


---

In my free time, you'll find me tinkering with embedded systems, experimenting with distributed systems, and playing around with radio frequencies. I love the challenge of figuring out how things work and finding innovative solutions to complex problems.

Having spent the latter half of my life between Iowa and Washington, D.C., I've been fortunate to gain diverse experiences and insights that have shaped my professional journey. Whether it's working on compliance projects or staying ahead of the latest cyber threats, I'm always eager to learn and grow in this ever-evolving field.

---

## Let's talk.

You can reach me at [`fana@coquads.com`][email] 


Note: The views and opinions expressed in this blog are strictly my own and do not reflect those of my employers or any contracting agencies I work with.

[email]: mailto:fana@coquads.com

