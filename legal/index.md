---
layout: post
title: The Fine Print (Legal)
subtitle: "Updated March 2015"
skip_footer: true
---

**ÄLG Logo** adapted from Tavern Sign designed by [Andrew Hainen][ah] from the [Noun Project][noun].

[ah]: http://www.thenounproject.com/ahainen
[noun]: http://www.thenounproject.com



The postings on this site are my own and don't necessarily represent my 
  employer’s positions, strategies or opinions.

&copy; 2010&ndash;2015, Fana Teffera
